
$(document).ready(function(){
 
var nb1 = 1;            //Nombre de lignes 
var nb2 = 1;            //Total de l'addition de tous les chiffres
var etat = 1;           //Pour savoir si c'est une addition ou une soustraction
function Total() {                      //Fonction qui calcul le total
        if(etat==1){    
                nb2 = nb2 + nb1;        //Le total vaut : Total + Numero de la case
        }
        else {
                nb2 = nb2 - nb1;        //Sinon on soustrait le numéro de la case précédente          
        } 
        $("#resultat td:first-child").html(nb2);        //On affiche le resultat dans le footer
}
$("#body td:first-child").html(nb1);    //Afficher le premier chiffre
$("#resultat td:first-child").html(nb2); //Afficher le total
$("#btnAdd").click(function() {         //Quand on clique sur le boutton ajouter
        $("#body tr:nth-child(" + nb1 +")").after("<tr><td>" + $("td").length + "</td></tr>");  //On ajoute une ligne en plus après celle précédente
        nb1 = nb1 + 1;                  //Ajout d'un chiffre
        etat = 1;                       //C'est une addition
        Total();                        //On appel la fonction
});
$("#btnSup").click(function() {         //Quand on clique sur le bouton supprimer
        if(nb1 > 1){                    //Seulement si le nombre est superieur a 1
                $("#body tr:nth-child(" + nb1 + ")").remove();  //On retire la ligne précédente
                etat = 0;               //C'est une soustraction
                Total();                //Appel de la fonction
                nb1 = nb1 - 1;          //Le nombre de case est celui qui précéde -1
        }
});
});
